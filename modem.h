#ifndef MODEM_DRIVER
#define MODEM_DRIVER

#include "modem_talk.h"
#include "nrf_serial.h"

/* DEFINE AT GENERIC COMMANDS */
#define AT_ECHO             "E"
#define AT_ECHON            "E0"

/* DEFINE AT HTTP COMMANDS */
#define AT_UHTTP            "UHTTP"
#define AT_UHTTPC           "UHTTPC"
#define AT_DWNBLK           "UDWNBLOCK"
#define AT_RDBLK            "URDBLOCK"
#define AT_LSTFILE          "ULSTFILE"
#define AT_DELFILE          "UDELFILE"

/* DEFINE AT HTTP SECONDARY COMMAND PARAMETERS */
#define AT_UHTTP_NAME       "1"
#define AT_UHTTP_SECURE     "6"
#define AT_UHTTP_SECUREV    "1"
#define AT_UHTTPC_GET       "1"
#define AT_UHTTPC_POST      "4"
#define AT_UHTTPC_PTEXT     "1"
#define AT_UHTTPC_JSON      "6"
#define AT_LSTFILE_SIZE     "2"

/* DEFINE HTTP COMMAND PAYLOAD PARAMETERS */
#define HTTP_PROFILE        "0"
#define HTTP_SECURE         "1"
#define HTTP_ADDR           "1ab01796809a.ngrok.io"
#define HTTP_ROUTE_INDEX    "/"
#define HTTP_ROUTE_UP       "/up"
#define HTTP_ROUTE_DOWN     "/down"
#define HTTP_F_UP           "\"up\""
#define HTTP_F_DOWN         "\"down\""
#define HTTP_F_UP_OFFSET    "0"
#define HTTP_F_DOWN_OFFSET  "0"

/* DEFINE HTTP INIT STATES */
enum http_init_state {
    HTTP_INIT_NONE,
    HTTP_INIT_ECHO,
    HTTP_INIT_IDLE,
    HTTP_INIT_BASE,
    HTTP_INIT_SECURE
};

/* DEFINE HTTP COMMUNICATION STATES */
enum http_state {
    HTTP_NOT_READY,    
    HTTP_READY,  
    HTTP_CLEAN_UP,  
    HTTP_SET_UP,
    HTTP_WRITE_UP,
    HTTP_SEND_UP,
    HTTP_LIST_DN,
    HTTP_READ_DN,
    HTTP_CLEAN_DN,     
    HTTP_IDLE,
    HTTP_EXIT
};

void modem_init(uint32_t pwr_pin, struct serial_interface* p_modem_serial, void (*p_debug)(void*,int));
bool modem_power_on();
bool modem_power_off();
bool modem_ready();
void modem_loop();
bool modem_http_init();
bool modem_http(const char* p_route, char* p_tx_data, int i_tx_len);
void modem_http_print(char** p_rx_data, int* p_rx_len) ;

#endif // MODEM_DRIVER