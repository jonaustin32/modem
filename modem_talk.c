// modem_talk.c

#include <stdarg.h>
#include <string.h>
#include "modem_talk.h"
#include "nrf_serial.h"
#include "boards.h"

#define BUFFER_RX_SIZE 4096
#define BUFFER_TX_SIZE 256

static void (*serial_debug)(void*,int);

static volatile enum at_resp at_response = ATR_IDLE;
static struct at_response atr;

static char rx_buffer[BUFFER_RX_SIZE];
static char tx_buffer[BUFFER_TX_SIZE];
static int rx_length = 0;
static int tx_length = 0;

/** PRIVATE FUNCTIONS **/

/* reset the buffer */
static void reset_buffer(char* buffer, int* buffer_length) {
    memset(buffer, '\0', *buffer_length);
    *buffer_length = 0;
}

/* generic function to check buffer for a token */
static bool check(char* arr, int length, const char* token) {
    if (rx_length >= length) {
        return (strnstr(arr, token, length)) ? true : false;
    } else {
        return false;
    }
}

/* check buffer for newline */
static bool check_newline() {
    return check(&rx_buffer[rx_length-2], strlen(AT_RESP_NEW), AT_RESP_NEW);
}

/* check buffer for OK */
static bool check_ok() {
    return check(&rx_buffer[rx_length-4], strlen(AT_RESP_OK), AT_RESP_OK) && !check(rx_buffer, strlen(AT_RESP_URDBLOCK), AT_RESP_URDBLOCK);
}

/* check buffer for prompt */
static bool check_prompt() {
    return check(&rx_buffer[rx_length-2], strlen(AT_RESP_PROMPT), AT_RESP_PROMPT);
}

/* check buffer for newline */
static bool check_uuhttpcr() {
    return check(rx_buffer, strlen(AT_RESP_UUHTTPCR), AT_RESP_UUHTTPCR);
}

/* check buffer for file listing */
static bool check_ulstfile() {
    return check(rx_buffer, strlen(AT_RESP_ULSTFILE), AT_RESP_ULSTFILE);
}

/* check buffer for file readout */
static bool check_urdblock() {    
    return check(rx_buffer, strlen(AT_RESP_URDBLOCK), AT_RESP_URDBLOCK)
           && check(&rx_buffer[rx_length-6], strlen("\r\nOK\r\n"), "\r\nOK\r\n"); 
}

/* check buffer for an error message */
static bool check_error() {
    if (check(rx_buffer, strlen(AT_ERR), AT_ERR)) {
        if (strstr(rx_buffer, AT_ERR_NOFILE)) {
            atr.err_msg = AT_ERR_NOFILE;
        }
        return true;
    } else {
        return false;
    }
}

/* function to load an AT command in the buffer */
static bool loadATCmd(const char* p_data) {
    if (p_data) {
        int length = strlen(p_data);
        if (tx_length + length > BUFFER_TX_SIZE) {
            return false;
        } else {
            strcpy(&tx_buffer[tx_length], p_data);
            tx_length += length;
            return true;
        }
    }
}

/** PUBLIC FUNCTIONS **/

/** 
 * @brief initialize the modem_talk driver
 *
 * @param pwr_pin           GPIO pin which manages power ON/OFF
 * @param p_modem_serial    pointer to serial bus struct as defined in modem_interface.h
 * @param p_debug           callback function for debugging (i.e. printing) driver, set to NULL if not debugging
 *
 * NOTE: This function will call modem_interface_init(), which will initialize the modem_interface driver.
 */
void modem_talk_init(uint32_t pwr_pin, struct serial_interface* p_modem_serial, void (*p_debug)(void*,int)) {
    at_response = ATR_IDLE;
    serial_debug = p_debug;
    atr.response = &at_response;
    atr.err_msg = NULL;
    atr.buffer = rx_buffer;
    atr.len = &rx_length;
    modem_interface_init(pwr_pin, p_modem_serial, p_debug);       
}

/** 
 * @brief callback function for inbound RX data
 *
 * @param p_serial          pointer to serial bus object
 * @param event             NRF serial event
 *
 * NOTE: Everytime the SERIAL_EVENT_RX_DATA event is registered, the data is appended to the RX buffer and the 
 * buffer is parsed for event tokens.
 */
void modem_talk_callback(struct nrf_serial_s const * p_serial, nrf_serial_event_t event) {           
    switch (event) {
        case NRF_SERIAL_EVENT_RX_DATA:          
            rx_length += modem_interface_recv(&rx_buffer[rx_length], BUFFER_RX_SIZE-rx_length);                         
            if (check_newline()) {
                if (check_urdblock()) {
                    at_response = ATR_URDBLOCK;
                } else if (check_ok()) {
                    if (check_ulstfile()) {
                        at_response = ATR_ULSTFILE;
                    } else {
                        at_response = ATR_NONE;
                    }                    
                } else if (check_uuhttpcr()) {
                    at_response = ATR_UUHTTPCR;
                } else if (check_error()) {                    
                    at_response = ATR_ERROR;
                }
            } else if(check_prompt()) {
                at_response = ATR_PROMPT;
            }
            break;
        case NRF_SERIAL_EVENT_TX_DONE:
            break;
        case NRF_SERIAL_EVENT_DRV_ERR:
            break;
        case NRF_SERIAL_EVENT_FIFO_ERR:
            break;
        default:
            break;
    }
}

/** 
 * @brief construct an AT command to be pulled into the TX buffer
 *
 * @param verb              AT verb
 * @param cmd               pointer to AT command string
 * @param firstArg (...)    [variadic] pointers to argument strings
 *
 * NOTE: Everytime the SERIAL_EVENT_RX_DATA event is registered, the data is appended to the RX buffer and the 
 * buffer is parsed for event tokens.
 */
void modem_talk_at(enum at_verb verb, const char* cmd, const char* firstArg, ...) {    
    // clear out the TX buffer, begin loading the AT command 
    reset_buffer(rx_buffer,&rx_length);
    reset_buffer(tx_buffer,&tx_length);    
    // load verbs/cmd into TX buffer
    switch (verb) {
        case ATV_CMD:
            loadATCmd(AT);
            loadATCmd(cmd);
            break;
        case ATV_ACT:
            loadATCmd(AT);
            loadATCmd(AT_OP_CMD);
            loadATCmd(cmd);        
            break;
        case ATV_TST: 
            loadATCmd(AT);
            loadATCmd(AT_OP_CMD);       
            loadATCmd(cmd);
            loadATCmd(AT_OP_SET);
            loadATCmd(AT_OP_GET);
            break;
        case ATV_SET:
            loadATCmd(AT);
            loadATCmd(AT_OP_CMD);       
            loadATCmd(cmd);
            loadATCmd(AT_OP_SET);
            break;
        case ATV_GET:
            loadATCmd(AT);
            loadATCmd(AT_OP_CMD);       
            loadATCmd(cmd);
            loadATCmd(AT_OP_GET);
            break;                        
        default:
            loadATCmd(cmd);
            break;               
    }
    // parse input strings    
    va_list args;
    va_start(args, firstArg);
    char arr[64];
    int i[1] = {0};
    for (const char* arg = firstArg; strcmp("\0",arg) != 0; arg = va_arg(args, const char*)) {    
        strcpy(&arr[i[0]],arg);
        i[0] += strlen(arg);
        loadATCmd(arg);
    }
    va_end(args);
    // newline and send
    loadATCmd(AT_RESP_NEW);
    serial_debug(tx_buffer, tx_length);
    at_response = ATR_PENDING;    
    modem_interface_send(tx_buffer,tx_length); 
}

/** 
 * @brief Return an at_response structure that contains volatile pointers to state/buffer information
 *
 * @return Pointer to at_response structure
 *
 * NOTE: This should only be called by the modem_init() function in modem.c
 */
struct at_response* modem_talk_resp() {
    return &atr;
}