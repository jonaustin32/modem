#ifndef MODEM_TALK_DRIVER
#define MODEM_TALK_DRIVER

#include "modem_interface.h"
#include "nrf_serial.h"

/* DEFINE AT VERBS */
enum at_verb {
    ATV_CMD,
    ATV_ACT,
    ATV_SET,
    ATV_GET,
    ATV_TST,
    ATV_NONE
};

/* DEFINE AT RESPONSES */
enum at_resp {
    ATR_IDLE,
    ATR_PENDING,
    ATR_NONE,
    ATR_OK,
    ATR_ERROR,
    ATR_PROMPT,
    ATR_UUHTTPCR,
    ATR_ULSTFILE,
    ATR_URDBLOCK
};

/* DEFINE AT RESPONSE STRUCTURE */
struct at_response {
    volatile enum at_resp* response;
    volatile const char* err_msg;
    char* buffer;
    int* len;
};

/* DEFINE AT OPERATORS AND RESPONSES  */
#define AT                  "AT"
#define AT_OP_CMD           "+"
#define AT_OP_SET           "="
#define AT_OP_GET           "?"   
#define AT_RESP_NEW         "\r\n"
#define AT_RESP_OK          "OK"
#define AT_RESP_PROMPT      "> "
#define AT_RESP_UUHTTPCR    "+UUHTTPCR: "
#define AT_RESP_ULSTFILE    "+ULSTFILE: "
#define AT_RESP_URDBLOCK    "+URDBLOCK: "

/* DEFINE AT ERRORS */
#define AT_ERR              "+CME ERROR: "
#define AT_ERR_NOFILE       "File not found"

/* DEFINE LTE COMMUNICATION VALUES */
#define SOCKET_TCP      "6"
#define SOCKET_UDP      "17"

void modem_talk_init(uint32_t pwr_pin, struct serial_interface* p_modem_serial, void (*p_debug)(void*,int));
void modem_talk_callback(struct nrf_serial_s const * p_serial, nrf_serial_event_t event);
void modem_talk_at(enum at_verb verb, const char* cmd, const char* firstArg, ...);
struct at_response* modem_talk_resp();

#endif // MODEM_TALK_DRIVER