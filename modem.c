// modem.c

#include "modem.h"
#include "app_timer.h"
#include "nrf_serial.h"
#include "boards.h"
#include <stdlib.h>

#define X_LEN_SIZE  10
#define F_LEN_SIZE  10

static void (*serial_debug)(void*,int);
static struct at_response* atr;

static enum http_init_state m_http_init_state = HTTP_INIT_NONE;
static enum http_state m_http_state = HTTP_NOT_READY;

static bool m_init = false;
static bool m_busy = false;
static int m_fsize = 0;

static char f_len[F_LEN_SIZE];
static char* f_len_ptr;
static char* f_trm_ptr;
static char* r_ptr;

static const char* route;
static char* tx_data;
static int tx_len;
char x_len[X_LEN_SIZE];

/** PRIVATE FUNCTIONS **/

/* send signal to turn the modem ON or OFF */
static bool modem_power(bool on) {
    if (m_init) {
        if (on) {
            if (modem_interface_down()) {
                modem_interface_boot();
                return true;
            }
        } else {
            if (modem_interface_ready()) {
                modem_interface_shutdown();
                return true;
            }
        }
    }
    return false;
}

/** PUBLIC FUNCTIONS **/

/** 
 * @brief initialize the modem driver
 *
 * @param pwr_pin           GPIO pin which manages power ON/OFF
 * @param p_modem_serial    pointer to serial bus struct as defined in modem_interface.h
 * @param p_debug           callback function for debugging (i.e. printing) driver, set to NULL if not debugging
 *
 * NOTE: This function will call modem_talk_init(), which will initialize the modem_talk driver.
 */
void modem_init(uint32_t pwr_pin, struct serial_interface* p_modem_serial, void (*p_debug)(void*,int)) {
    serial_debug = p_debug;    
    modem_talk_init(pwr_pin, p_modem_serial, p_debug);
    atr = modem_talk_resp();
    m_init = true;
}

/** 
 * @brief Power ON the modem
 *
 * @return True if command accepted, False if already processing another command or modem interface is not ready
 */
bool modem_power_on() {
    return modem_power(true);    
}

/** 
 * @brief Power OFF the modem
 *
 * @return True if command accepted, False if already processing another command or modem interface is not ready
 */
bool modem_power_off() {
    return modem_power(false);
}

/** 
 * @brief Initialize the modem
 *
 * @return True if command accepted, False if already processing another command or modem interface is not ready
 */
bool modem_ready() {
    return (modem_interface_ready() && !m_busy) ? true : false;
}

/** 
 * @brief Handle modem events (i.e. modem state machine)
 *
 * NOTE: This should be called at least once per system loop.
 *
 * NOTE: The first level of modem_loop() is a modem response handler state machine. In order to issue a command to the modem, the response handler
 * state machine must transition to the ATR_IDLE state. In this state, a second level state machine checks if the HTTP interface has been initialized
 * and secured. Once the HTTP_INIT_SECURE state is reached, the third-level HTTP state machine handles modem uplink/downlink file management and HTTP 
 * post/response.
 *
 * NOTE: The malloc call in the ATR_URDBLOCK state corresponds to a free call in the modem_http() call. These calls will be replaced with a callback
 * function.
 */
void modem_loop() {
    switch (*atr->response) {
        case ATR_NONE: // no response
            *atr->response = ATR_IDLE;    
            break;
        case ATR_ERROR: // error response, apply http_state specific error handling here
            *atr->response = ATR_IDLE;  
            if (atr->err_msg == AT_ERR_NOFILE) {
                if (m_http_state == HTTP_SET_UP) {                                      
                    break;
                }                
            }
            m_http_state = HTTP_EXIT;
            break;
        case ATR_PROMPT: // prompt response, modem is waiting for input
            *atr->response = ATR_IDLE;        
            break;
        case ATR_UUHTTPCR: // http command response, either asking modem or send or modem has gotten an HTTP response
            if (atr->buffer[*atr->len - 3] == '1') {
                m_http_state = HTTP_LIST_DN;
            } else {
                m_http_state = HTTP_SEND_UP;
            }
            *atr->response = ATR_IDLE;        
            break;
        case ATR_ULSTFILE: // list file response, allows host processor to obtain filesize          
            f_len_ptr = strstr(atr->buffer, AT_RESP_ULSTFILE) + strlen(AT_RESP_ULSTFILE);
            f_trm_ptr = strstr(atr->buffer, "\r\n\r\nOK\r\n");
            memset(f_len, '\0', F_LEN_SIZE);
            memcpy(f_len, f_len_ptr, f_trm_ptr - f_len_ptr);
            m_fsize = atoi(f_len);            
            m_http_state = HTTP_READ_DN;
            *atr->response = ATR_IDLE;  
            break;
        case ATR_URDBLOCK: // read file response, obtain the http response
            f_len_ptr = strstr(atr->buffer, f_len) + strlen((const char*)f_len) + strlen(",\"");
            f_trm_ptr = strstr(atr->buffer, "\"\r\nOK\r\n");   
            r_ptr = (char*) malloc(m_fsize); // refer to NOTE
            memcpy(r_ptr, f_len_ptr, m_fsize);                 
            m_http_state = HTTP_CLEAN_DN;
            *atr->response = ATR_IDLE;
            break;
        case ATR_IDLE: // 
            if (m_busy) { // if modem is in the middle of either modem_http, modem_http_init
                switch (m_http_init_state) { // 
                    case HTTP_INIT_ECHO: // disable echo
                        modem_talk_at(ATV_CMD, AT_ECHON, "\0");
                        m_http_init_state = HTTP_INIT_IDLE;
                        break;
                    case HTTP_INIT_IDLE: // configure HTTP profile and base address
                        modem_talk_at(ATV_SET, AT_UHTTP, HTTP_PROFILE, ",", AT_UHTTP_NAME, ",", "\"", HTTP_ADDR, "\"", "\0");
                        m_http_init_state = HTTP_INIT_BASE;
                        break;
                    case HTTP_INIT_BASE: // configure HTTP security
                        modem_talk_at(ATV_SET, AT_UHTTP, HTTP_PROFILE, ",", AT_UHTTP_SECURE, ",", AT_UHTTP_SECUREV, "\0");
                        m_http_init_state = HTTP_INIT_SECURE;
                        break;
                    case HTTP_INIT_SECURE: // modem is ready to use https                                                                                                          
                        switch (m_http_state) {
                            case HTTP_NOT_READY: // this state will only be visited after initialization and first http command
                                m_http_state = HTTP_READY; 
                                m_busy = false;                               
                                break;
                            case HTTP_CLEAN_UP: // clean up any previous uplink files if present                               
                                modem_talk_at(ATV_SET, AT_DELFILE, HTTP_F_UP, "\0");
                                m_http_state = HTTP_SET_UP;
                                break;  
                            case HTTP_SET_UP: // ask to write to uplink file                              
                                memset(x_len, '\0', X_LEN_SIZE);
                                sprintf(x_len, "%d", tx_len);
                                modem_talk_at(ATV_SET, AT_DWNBLK, HTTP_F_UP, ",", HTTP_F_UP_OFFSET, ",", (const char*)x_len, ",", (const char*)x_len, "\0");
                                m_http_state = HTTP_WRITE_UP;                        
                                break;
                            case HTTP_WRITE_UP: // write payload to uplink file
                                modem_talk_at(ATV_NONE, NULL, (const char*)tx_data, "\0");
                                m_http_state = HTTP_SEND_UP;
                                break;
                            case HTTP_SEND_UP: // perform HTTP operation with contents of uplink file
                                modem_talk_at(ATV_SET, AT_UHTTPC, HTTP_PROFILE, ",", AT_UHTTPC_POST, ",", "\"", route, "\"", ",", HTTP_F_DOWN, ",", HTTP_F_UP, ",", AT_UHTTPC_PTEXT,"\0");
                                m_http_state = HTTP_IDLE;
                                break;
                            case HTTP_LIST_DN: // list the availabile downlink file
                                modem_talk_at(ATV_SET, AT_LSTFILE, AT_LSTFILE_SIZE, ",", HTTP_F_DOWN);
                                m_http_state = HTTP_IDLE;
                                break;
                            case HTTP_READ_DN: // read the downlink file
                                modem_talk_at(ATV_SET, AT_RDBLK, HTTP_F_DOWN, ",", HTTP_F_DOWN_OFFSET, ",", (const char*)f_len, "\0");                                
                                m_http_state = HTTP_IDLE;
                                break;
                            case HTTP_CLEAN_DN: // clean up the downlink file
                                modem_talk_at(ATV_SET, AT_DELFILE, HTTP_F_DOWN, "\0");                                                                
                                m_http_state = HTTP_EXIT;                                
                                break;    
                            case HTTP_EXIT: // exit state (needed to confirm downlink cleanup)
                                m_http_state = HTTP_READY;
                                m_busy = false;
                                break;                                                   
                            default:
                                break;
                        }                       
                        break;
                    default:
                        break;        
                }
            }
            break;   
        default:
            break;
    }
}

/** 
 * @brief Initialize the modem HTTP interface
 *
 * @return True if command accepted, False if already processing another command or modem_http_init() has already been run
 */
bool modem_http_init() {       
    if (!m_busy && m_http_init_state == HTTP_INIT_NONE) {
        m_http_init_state = HTTP_INIT_ECHO;
        m_busy = true;
        return true;
    } else {
        return false;
    }        
}

/** 
 * @brief Performs an HTTP Post command
 *
 * @param p_route   pointer to string containing the route (e.g. "/up" if the target addr is https://medicasafe.com/up
 * @param p_tx_data pointer to char buffer containing the payload
 * @param i_tx_len  length of the payload
 *
 * @return True if command accepted, False if already processing another command or modem_http_init() hasn't been run yet
 *
 * NOTE: The call to 'free' in this function is paired with the ATR_URDBLOCK state in the modem_loop() function - these will be replaced
 * with a callback function (e.g. callback to write to application-specified buffer, in flash etc.)
 */
bool modem_http(const char* p_route, char* p_tx_data, int i_tx_len) {
    if (!m_busy && m_http_state == HTTP_READY) {
        route = p_route;
        tx_data = p_tx_data;
        tx_len = i_tx_len;
        m_http_state = HTTP_CLEAN_UP;
        free(r_ptr);
        m_busy = true;
        return true;
    } else {
        return false;
    }
}

/** 
 * @brief Return the results of the most recent HTTP Post commmand
 *
 * @param p_rx_data double pointer to be populated with head of response buffer
 * @param p_rx_len  pointer to be populated with length of response buffer
 *
 * NOTE: This will be replaced with a callback function as mentioned in the NOTE for the modem_http() function.
 */
void modem_http_print(char** p_rx_data, int* p_rx_len) { 
    *p_rx_data = r_ptr;
    *p_rx_len = m_fsize;
}