#ifndef MODEM_INTERFACE_DRIVER
#define MODEM_INTERFACE_DRIVER

#include "nrf_serial.h"

/* DEFINE SERIAL INTERFACE STRUCTURE */
struct serial_interface {
    nrf_serial_t const * p_serial;
    nrf_drv_uart_config_t const * p_drv_uart_config;
    nrf_serial_config_t const * p_config;
};

/* DEFINE MODEM FUNCTIONAL STATES */
enum modem_state {
    MODEM_NO_POWER,
    MODEM_POWER,
    MODEM_READY,
    MODEM_SHUTDOWN
};

void modem_interface_init(uint32_t pwr_pin, struct serial_interface* p_modem_serial, void (*p_debug)(void*,int));
bool modem_interface_ready();
bool modem_interface_down();
void modem_interface_boot();
void modem_interface_shutdown();
void modem_interface_send(void* p_data, int len);
int modem_interface_recv(void* p_data, int len);

/* FOR DEBUG PURPOSES
static char a[] = "a\r\n";
static char b[] = "b\r\n";
static char c[] = "c\r\n";
static char d[] = "d\r\n";
static char e[] = "e\r\n";
static char tx_message[] = "AT\r\n";
*/

#endif // MODEM_INTERFACE_DRIVER