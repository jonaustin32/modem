Contents:

main.c: application code for using high-level functions in the modem.*
modem.*: contains code for high-level functions (e.g. initialize HTTP, send over HTTP)
modem_talk.*: contains code to construct AT commands and translate recieved messages into modem state
modem_interface.*: contains code to set up modem GPIO, configure serial bus, and perform modem RX/TX

Functionality:

General steps for using this driver to transmit over HTTP:

- To initialize the modem drivers, pass the following arguments to modem_init(...)
    - Define the UART Interface (e.g. NRF_SERIAL_UART_DEF)
    - Define the UART Driver configs (e.g. NRF_SERIAL_DRV_UART_CONFIG_DEF)
    - Define the Serial configs (e.g. NRF_SERIAL_CONFIG_DEF)
    - Define the GPIO pin which controls power state of the modem

- Run modem_loop() every system loop to process modem events

- Run modem_ready() to check if modem is up and is not occupied running another command
    - If TRUE, then you can run one of the following commands
    - Upon running a command, modem_ready() will be FALSE
    - modem_ready() will return TRUE when the command is complete

- Run modem_power_on() for power up and boot

- Run modem_power_off() to shutdown and power off

- Every time the modem is turned on, run modem_http_init() to configure HTTP

- To send data, pass the following arguments to modem_http(...)
    - route (NOTE: base address can be modified via HTTP_ADDR in modem.h)
    - payload buffer
    - payload length

- To fetch data, pass the following arguments to modem_http_print(...)
    - pointer to a receiving buffer
    - length of recieving buffer

Notes about Driver Usage

- at least 3 seconds should pass between calling modem_off() and modem_on()
    - in the application code, there should likely never be an instance where modem_off() and modem_on() are called in quick succession
- after all data commands are processed, call modem_off() ASAP
    - the modem will go to sleep after ~5 seconds of inactivity which cause issues with tracking modem state
- to work with the nRF52840, the GPIO had to be set to 3.3V
    - please refer to https://devzone.nordicsemi.com/f/nordic-q-a/50466/how-to-use-gpio-pins-on-3-3v

Continuing Work

- Replacing the modem_http_print() and free/malloc calls with an application-defined callback to write to a designated memory location
- Configure the driver to work with the official MedicaSafe HTTP endpoints
- Akin to the HTTP Initialization and HTTP Post sequence, create a sequence for provisioning modem at the factory
    - For cybersecurity, an extension of this provisioning system may include fetching MODEM/SIM serial numbers
- Better defined error handling (e.g. modem errors may occur depending on environment of device use, will require field testing)

