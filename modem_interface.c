// modem_interface.c

#include "modem_interface.h"
#include "app_timer.h"
#include "nrf_serial.h"
#include "nrf_gpio.h"
#include "boards.h"

#define MODEM_INTERVAL_ON   300
#define MODEM_INTERVAL_BOOT 7000
#define MODEM_INTERVAL_OFF  2000

APP_TIMER_DEF(m_modem_on_timer_id);
APP_TIMER_DEF(m_modem_boot_timer_id);
APP_TIMER_DEF(m_modem_off_timer_id);

ret_code_t ret;
static void (*serial_debug)(void*,int);

static volatile enum modem_state m_state = MODEM_NO_POWER;

static uint32_t pin_modem_power;
static struct serial_interface* serial_modem;
static bool is_driver_init = false;

/** PRIVATE FUNCTIONS **/

/* handler to shut off modem */
static void modem_off_handler(void* p_context) {
    nrf_gpio_pin_set(pin_modem_power);
    m_state = MODEM_NO_POWER;
}

/* handler to boot modem and serial bus */
static void modem_boot_handler(void* p_context) {
    ret = nrf_serial_init(serial_modem->p_serial, serial_modem->p_drv_uart_config, serial_modem->p_config);
    APP_ERROR_CHECK(ret);        
    m_state = MODEM_READY;
}

/* handler to power up modem */
static void modem_on_handler(void* p_context) {
    nrf_gpio_pin_set(pin_modem_power);
    ret = app_timer_start(m_modem_boot_timer_id, APP_TIMER_TICKS(MODEM_INTERVAL_BOOT), NULL);
    APP_ERROR_CHECK(ret);   
}

/** PUBLIC FUNCTIONS **/

/** 
 * @brief Initialize the modem interface driver
 *
 * @param pwr_pin           GPIO pin which manages power ON/OFF
 * @param p_modem_serial    pointer to serial bus struct as defined in modem_interface.h
 * @param p_debug           callback function for debugging (i.e. printing) driver, set to NULL if not debugging
 */
void modem_interface_init(uint32_t pwr_pin, struct serial_interface* p_modem_serial, void (*p_debug)(void*,int)) {
    // init power pin, serial interfaces
    pin_modem_power = pwr_pin;
    serial_modem = p_modem_serial;
    serial_debug = p_debug;
    // create modem state timers
    ret = app_timer_create(&m_modem_on_timer_id, APP_TIMER_MODE_SINGLE_SHOT, modem_on_handler);
    APP_ERROR_CHECK(ret);
    ret = app_timer_create(&m_modem_boot_timer_id, APP_TIMER_MODE_SINGLE_SHOT, modem_boot_handler);
    APP_ERROR_CHECK(ret);
    ret = app_timer_create(&m_modem_off_timer_id, APP_TIMER_MODE_SINGLE_SHOT, modem_off_handler);
    APP_ERROR_CHECK(ret); 
    // driver is initialized
    is_driver_init = true;                
    return;
}

/** 
 * @brief Check if modem interface is ready
 *
 * @return True if in MODEM_READY state, False otherwise
 */
bool modem_interface_ready() {
    return (m_state == MODEM_READY) ? true : false;
}

/** 
 * @brief Check if modem interface is down
 *
 * @return True if in MODEM_NO_POWER state, False otherwise
 */
bool modem_interface_down() {
    return (m_state == MODEM_NO_POWER) ? true : false;
}

/** 
 * @brief Send data to modem via the modem interface
 *
 * @param p_data    pointer to data buffer
 * @param len       length of data buffer
 */
void modem_interface_send(void* p_data, int len) {
    if (modem_interface_ready()) {        
        ret = nrf_serial_write(serial_modem->p_serial, p_data, len, NULL, 0);
        APP_ERROR_CHECK(ret);
        (void)nrf_serial_flush(serial_modem->p_serial, 0);
    }
}

/** 
 * @brief Recieve data from modem via the modem interface
 *
 * @param p_data    pointer to data buffer
 * @param len       length of data buffer
 *
 * NOTE: The NRF_ERROR_TIMEOUT return code should be ignored (recommended on Nordic DevZone).
 */
int modem_interface_recv(void* p_data, int len) {
    int bytes_read = 0;    
    if (modem_interface_ready()) {    
        ret = nrf_serial_read(serial_modem->p_serial, p_data, len, &bytes_read, 0);
        if (ret != NRF_ERROR_TIMEOUT) {
            APP_ERROR_CHECK(ret);
        }                   
    }
    return bytes_read;
}

/** 
 * @brief Boot the modem interface
 *
 * NOTE: This function will kick off a timer to power on the modem (e.g. set pin LOW, let timer run for 300ms, set pin HI
 * in the callback). In the callback, delay another 7000ms to permit the modem to complete its boot process and then
 * initialize the serial bus interface.
 */
void modem_interface_boot() {
    if (is_driver_init && m_state == MODEM_NO_POWER) {                                         
        m_state = MODEM_POWER;            
        nrf_gpio_pin_clear(pin_modem_power);
        ret = app_timer_start(m_modem_on_timer_id, APP_TIMER_TICKS(MODEM_INTERVAL_ON), NULL);
        APP_ERROR_CHECK(ret);                   
    }
}

/** 
 * @brief Shutdown the modem interface
 *
 * NOTE: This function will first uninitialize the serial interface. The function will then kick off a timer to power off the 
 * modem (e.g. set pin LOW, let timer run for 2000ms, set pin HI in the callback). 
 */
void modem_interface_shutdown() {
    if (is_driver_init && m_state == MODEM_READY) {
        ret = nrf_serial_uninit(serial_modem->p_serial);
        APP_ERROR_CHECK(ret);        
        m_state = MODEM_SHUTDOWN;
        nrf_gpio_pin_clear(pin_modem_power);
        ret = app_timer_start(m_modem_off_timer_id, APP_TIMER_TICKS(MODEM_INTERVAL_OFF), NULL);
        APP_ERROR_CHECK(ret);                
    }    
}